import sys
from pathlib import Path
from typing import List

import pandas as pd

from evaluation import save_predictions
from models import AutoArima, BaseLineModel, FeedForward, LSTMCustom

if __name__ == "__main__":
    # ------------------------------------------------------------------------------------------------------------------
    # Load Data
    secret_training_data = pd.read_csv(Path("datasets") / "secret_training_data.csv")
    times: List[str] = pd.read_csv("datasets/public_fake_test_data.csv").times.tolist()
    # ------------------------------------------------------------------------------------------------------------------
    # Select model
    models = {
        "arima": AutoArima(),
        "baseline": BaseLineModel(),
        "feed_forward": FeedForward(n_time_series=len(secret_training_data.columns) - 1, sequence_length=150),
        "lstm": LSTMCustom(n_time_series=len(secret_training_data.columns) - 1, sequence_length=10, hidden_units=64),
    }
    _, model_str, out_file = sys.argv
    model = models[model_str]
    # ------------------------------------------------------------------------------------------------------------------
    # Train model with secret training data
    model.fit(secret_training_data)
    # ------------------------------------------------------------------------------------------------------------------
    # Predict and save
    predictions = model.predict(len(times))
    save_predictions(predictions, times, cols=secret_training_data.columns.tolist(), file_path=Path(out_file))
    # ------------------------------------------------------------------------------------------------------------------
