import math
from typing import Any, List

import numpy
import numpy as np
import pandas as pd
import pmdarima as pm
from more_itertools import batched
from tqdm import tqdm

from models.model import TimeSeriesModel


class AutoArima(TimeSeriesModel):
    def __init__(self, day_in_month: int = 31):
        TimeSeriesModel.__init__(self)
        self.arima_list: List[Any] = []
        self.days_in_month = day_in_month

    def fit(self, data_frame: pd.DataFrame) -> None:
        data = self.default_preprocess(data_frame)
        progress_bar = tqdm(data_frame.drop("times", axis=1).columns)
        for col_index, column in enumerate(progress_bar):
            progress_bar.set_description(f"Precess {column}:")
            monthly_data = np.array([np.median(batch) for batch in batched(data[:, col_index], self.days_in_month)])

            self.arima_list.append(
                pm.auto_arima(monthly_data, error_action='ignore', trace=False,
                              suppress_warnings=True, maxiter=50,
                              seasonal=True, m=round(365 / self.days_in_month))
            )

    def predict(self, n_predictions: int):
        predictions = np.zeros((n_predictions, len(self.arima_list)))
        for col, arima in enumerate(self.arima_list):
            monthly_predicts = arima.predict(n_periods=math.ceil(n_predictions / self.days_in_month) + 2)
            seasonal_predicts = []
            for x in range(n_predictions):
                x1 = math.floor(x / self.days_in_month)
                x2 = x1 + 1
                lambda_ = (x % self.days_in_month) / self.days_in_month
                # near to 0 -> more of month x1, near to 1 -> more of month x2
                seasonal_predicts.append((1 - lambda_) * monthly_predicts[x1] + lambda_ * monthly_predicts[x2])

            predictions[:, col] = numpy.array(seasonal_predicts)
        return predictions
