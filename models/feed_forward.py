from pathlib import Path
from typing import Any

import numpy.typing as npt
import pandas as pd
import torch
from torch.nn import Sequential, Linear, ReLU, Flatten, Module

from models.model import TimeSeriesModel
from models.torch_model_adapter import TorchModelAdapter


class FeedForward(TimeSeriesModel):
    def __init__(self, n_time_series: int, sequence_length: int = 5, train_history_path: Path = None):
        self.model_adapter = TorchModelAdapter(
            model=TorchFeedForward(n_time_series=n_time_series, sequence_length=sequence_length),
            n_time_series=n_time_series,
            sequence_length=sequence_length,
            train_history_path=train_history_path,
        )

    def fit(self, data_frame: pd.DataFrame) -> None:
        self.model_adapter.fit(data_frame)

    def predict(self, n_predictions: int) -> npt.NDArray[Any]:
        return self.model_adapter.predict(n_predictions)


class TorchFeedForward(Module):
    def __init__(self, n_time_series: int, sequence_length: int = 5):
        super().__init__()
        width = 100
        self.net = Sequential(
            Flatten(),
            Linear(in_features=n_time_series * sequence_length, out_features=width),
            ReLU(),
            Linear(in_features=width, out_features=width),
            ReLU(),
            Linear(in_features=width, out_features=width),
            ReLU(),
            Linear(in_features=width, out_features=width),
            ReLU(),
            Linear(in_features=width, out_features=width),
            ReLU(),
            Linear(in_features=width, out_features=n_time_series)
        )

    def forward(self, _inputs: torch.Tensor) -> torch.Tensor:
        return self.net(_inputs.to(torch.float32))
