from typing import Tuple, Any

import numpy.typing as npt
import torch
from torch.utils.data import Dataset


class SequenceDataset(Dataset):
    def __init__(self, data: npt.NDArray[Any], target: npt.NDArray[Any], sequence_length: int = 5):
        self.sequence_length = sequence_length
        self.X = torch.tensor(data).float()
        self.y = torch.tensor(target).float() if target is not None else None

    def __len__(self):
        return self.X.shape[0]

    def __getitem__(self, key: int) -> Tuple[torch.tensor, torch.tensor]:
        """:returns a tuple of the sequence data [key - sequence_length... key) and target[key]"""
        if key >= self.sequence_length:
            i_start = key - self.sequence_length
            x = self.X[i_start:key, :]
        else:
            padding = self.X[0].repeat(self.sequence_length - key, 1)
            x = self.X[0:key, :]
            x = torch.cat((padding, x), 0)
        y = self.y[key] if self.y is not None else None
        return x, y

    def __setitem__(self, key: int, value: npt.NDArray[Any]) -> None:
        self.X[key] = value
