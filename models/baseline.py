import numpy as np
import pandas as pd

from models.model import TimeSeriesModel


class BaseLineModel(TimeSeriesModel):
    def __init__(self):
        TimeSeriesModel.__init__(self)
        self.means = None
        self.time_series_axis = 0

    def fit(self, data_frame: pd.DataFrame) -> None:
        data = self.default_preprocess(data_frame)
        self.means = np.mean(data, axis=self.time_series_axis)[np.newaxis]

    def predict(self, n_predictions: int):
        return np.tile(self.means, (n_predictions, 1))
