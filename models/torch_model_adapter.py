from pathlib import Path
from typing import Optional, Union, List, Tuple, Any

import numpy as np
import numpy.typing as npt
import pandas as pd
import torch
import tqdm as tqdm
from sklearn.model_selection import train_test_split
from torch.nn import L1Loss, SmoothL1Loss, HuberLoss, MSELoss, Module
from torch.optim import Optimizer
from torch.utils.data import DataLoader
from tqdm import tqdm

from evaluation import score
from models.model import TimeSeriesModel
from models.sequence_dataset import SequenceDataset


class TorchModelAdapter(TimeSeriesModel):
    def __init__(self, model: Module, n_time_series: int, sequence_length: int = 5, train_history_path: Path = None):
        self.n_time_series = n_time_series
        self.sequence_length = sequence_length
        self.train_history_path = train_history_path
        self.adapter = TorchModelTrainer(
            model=model,
        )
        self.sequence_for_predictions = None

    def fit(self, data_frame: pd.DataFrame) -> None:
        data = self.default_preprocess(data_frame)
        self.sequence_for_predictions = data[-self.sequence_length:]
        train, validation = train_test_split(data, train_size=0.8, shuffle=False)
        train_dataloader = DataLoader(
            SequenceDataset(train, target=train, sequence_length=self.sequence_length),
            batch_size=1000,
        )
        validation_dataloader = DataLoader(
            SequenceDataset(validation, target=validation, sequence_length=self.sequence_length),
            batch_size=1000,
        )
        train_loss_hist, test_loss_hist, train_score_hist, test_score_hist = self.adapter.train(
            train_dataloader=train_dataloader,
            test_dataloader=validation_dataloader,
            num_epochs=500
        )
        if self.train_history_path:
            df = pd.DataFrame(columns=["train_loss", "test_loss", "train_score", "test_score"])
            df["train_loss"] = train_loss_hist
            df["test_loss"] = test_loss_hist
            df["train_score"] = train_score_hist
            df["test_score"] = test_score_hist
            self.train_history_path.parent.mkdir(parents=True, exist_ok=True)
            df.to_csv(self.train_history_path)

    def predict(self, n_predictions: int) -> npt.NDArray[Any]:
        sequence_data = SequenceDataset(
            data=np.vstack((self.sequence_for_predictions, np.zeros((n_predictions, self.n_time_series)))),
            target=None,
            sequence_length=self.sequence_length,
        )
        return self.adapter.predict(sequence_data)


class TorchModelTrainer:
    def __init__(
            self,
            model: Module,
            optimizer: Optional[Optimizer] = None,
            loss_function: Union[L1Loss, SmoothL1Loss, HuberLoss, MSELoss] = torch.nn.HuberLoss()
    ) -> None:
        self.model: Module = model
        self.optimizer: Optimizer = optimizer if optimizer else torch.optim.Adam(model.parameters(), lr=0.02,
                                                                                 weight_decay=0.001)
        self.loss_function: Union[L1Loss, SmoothL1Loss, HuberLoss, MSELoss] = loss_function

    def _run_epoch(self, dataloader: DataLoader, update_weights=True):
        epoch_loss = 0.0
        score_ = 0.0
        for _input, target in dataloader:
            if update_weights:
                self.optimizer.zero_grad()
            out = self.model.forward(_input)
            torch_loss = self.loss_function.forward(input=out, target=target)
            if update_weights:
                torch_loss.backward()
                self.optimizer.step()
            epoch_loss += float(torch_loss)
            score_ += score(target.detach().numpy(), out.detach().numpy())

        return epoch_loss / len(dataloader), score_ / len(dataloader)

    def train(
            self,
            train_dataloader: DataLoader,
            test_dataloader: DataLoader,
            num_epochs: int = 5,
            early_stopping_rounds: int = 50
    ) -> Tuple[List[float], List[float], List[float], List[float]]:
        self.model.train()
        train_loss_hist, test_loss_hist = [], []
        train_score_hist, test_score_hist = [], []
        best_loss = float('inf')
        best_weights = []
        no_improvement_count = 0
        progress_bar = tqdm(range(num_epochs))
        for epoch in progress_bar:
            self.model.train()
            train_loss, train_score = self._run_epoch(train_dataloader, update_weights=True)
            with torch.no_grad():
                self.model.eval()
                test_loss, test_score = self._run_epoch(test_dataloader, update_weights=False)
            train_loss_hist.append(train_loss)
            test_loss_hist.append(test_loss)
            train_score_hist.append(train_score)
            test_score_hist.append(test_score)
            progress_bar.set_description(
                f"Epoch: {epoch}, {train_loss=:.3f}, {train_loss=:.3f}, {train_score=:.3f}, {test_score=:.3f}"
            )
            if test_loss_hist[-1] <= best_loss:
                best_loss = test_loss_hist[-1]
                best_weights = self.model.state_dict()
                no_improvement_count = 0
            else:
                no_improvement_count += 1

            if no_improvement_count == early_stopping_rounds:
                self.model.load_state_dict(best_weights)
                break
        return train_loss_hist, test_loss_hist, train_score_hist, test_score_hist

    def predict(self, data: SequenceDataset) -> np.array:
        self.model.eval()
        with torch.no_grad():
            for i in tqdm(range(len(data)), desc="Predicting"):
                if i < data.sequence_length:
                    continue
                pred = self.model.forward(torch.unsqueeze(data[i][0], dim=0))
                data[i] = pred
            return data.X[data.sequence_length:].numpy()
