from typing import Any

import numpy.typing as npt
import pandas as pd


class TimeSeriesModel:

    @staticmethod
    def default_preprocess(data_frame: pd.DataFrame) -> npt.NDArray[Any]:
        return data_frame.drop('times', axis=1).to_numpy()

    def fit(self, data_frame: pd.DataFrame) -> None:
        pass

    def predict(self, n_predictions: int) -> npt.NDArray[Any]:
        pass
