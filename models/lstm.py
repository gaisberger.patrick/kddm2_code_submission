from pathlib import Path
from typing import Any

import numpy.typing as npt
import pandas as pd
import torch
from torch.nn import Linear, Module, LSTM

from models.model import TimeSeriesModel
from models.torch_model_adapter import TorchModelAdapter


class LSTMCustom(TimeSeriesModel):
    def __init__(
            self,
            n_time_series: int,
            hidden_units: int,
            sequence_length: int = 5,
            train_history_path: Path = None
    ):
        self.model_adapter = TorchModelAdapter(
            model=TorchLSTMCustom(
                n_time_series=n_time_series,
                hidden_units=hidden_units,
                out_features=n_time_series,
                sequence_length=sequence_length,
            ),
            n_time_series=n_time_series,
            sequence_length=sequence_length,
            train_history_path=train_history_path,
        )

    def fit(self, data_frame: pd.DataFrame) -> None:
        self.model_adapter.fit(data_frame)

    def predict(self, n_predictions: int) -> npt.NDArray[Any]:
        return self.model_adapter.predict(n_predictions)


class TorchLSTMCustom(Module):
    def __init__(self, n_time_series: int, hidden_units: int, out_features: int, sequence_length: int) -> None:
        super().__init__()
        self.n_time_series = n_time_series  # this is the number of features
        self.hidden_units = hidden_units
        self.num_layers = sequence_length

        self.lstm = LSTM(
            input_size=n_time_series,
            hidden_size=hidden_units,
            batch_first=True,
            num_layers=self.num_layers
        )
        self.linear = Linear(in_features=self.hidden_units, out_features=out_features)

    def forward(self, x):
        batch_size = x.shape[0]
        h0 = torch.zeros(self.num_layers, batch_size, self.hidden_units).requires_grad_()
        c0 = torch.zeros(self.num_layers, batch_size, self.hidden_units).requires_grad_()

        _, (hn, _) = self.lstm(x, (h0, c0))
        return self.linear(hn[0])
