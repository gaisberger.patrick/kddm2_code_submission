import math
import sys
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import pandas as pd
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.seasonal import seasonal_decompose

### GLOBAL RC SWITCH
SHOW_PLOT = False


def plot_differencing(df: pd.DataFrame) -> None:
    n_cols = len(df.columns) - 1
    n_rows = math.ceil(n_cols / 8)
    n_cols = 8
    fig, axs = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(20, 15))
    axs = axs.ravel()
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    fig.suptitle('Order Differencing for Time Series')
    for i, col in enumerate(df.columns[1:]):
        axs[i].plot(df[col].diff())
        axs[i].set_title(col)
        if i >= n_cols * n_rows - 1:
            break

    fig.tight_layout()
    fig.savefig('out/figures/1order_differencing.png')
    if SHOW_PLOT:
        plt.show()


def plot_correlation(df: pd.DataFrame) -> None:
    n_cols = len(df.columns) - 1
    n_rows = math.ceil(n_cols / 8)
    n_cols = 8
    fig, axs = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(20, 15))
    axs = axs.ravel()
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    fig.suptitle('Autocorrelation for Time Series')
    for i, col in enumerate(df.columns[1:]):
        plot_acf(df[col], ax=axs[i], auto_ylims=True)
        axs[i].set_title(col)
        if i >= n_cols * n_rows - 1:
            break

    fig.tight_layout()
    fig.savefig('out/figures/autocorrelation.png')
    if SHOW_PLOT:
        plt.show()

    ## PARTIAL
    fig, axs = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(20, 15))
    axs = axs.ravel()
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    fig.suptitle('Partial Autocorrelation for Time Series')
    for i, col in enumerate(df.columns[1:]):
        plot_pacf(df[col], ax=axs[i], auto_ylims=True)
        axs[i].set_title(col)
        if i >= n_cols * n_rows - 1:
            break

    fig.tight_layout()
    fig.savefig('out/figures/partial_autocorrelation.png')
    if SHOW_PLOT:
        plt.show()


def plot_seasonal_decompose(df: pd.DataFrame) -> None:
    n_cols = len(df.columns) - 1
    n_rows = math.ceil(n_cols / 8)
    n_cols = 8
    fig, axs = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(30, 20))
    axs = axs.ravel()
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    fig.suptitle('Observed Seasonal Decomposition for Time Series')

    fig2, axs2 = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(30, 20))
    axs2 = axs2.ravel()
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    fig2.suptitle('Trend Seasonal Decomposition for Time Series')

    fig3, axs3 = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(30, 20))
    axs3 = axs3.ravel()
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    fig3.suptitle('Residual Seasonal Decomposition for Time Series')

    new_df = df.copy()
    new_df.set_index('times', inplace=True)
    new_df.index = pd.to_datetime(new_df.index)
    new_df.dropna(inplace=True)

    for i, col in enumerate(df.columns[1:]):
        decomposition = seasonal_decompose(new_df[col], model="multiplicable")
        decomposition.observed.plot(ax=axs[i], legend=True)
        decomposition.trend.plot(ax=axs2[i])
        decomposition.resid.plot(ax=axs3[i])
        axs2[i].set_title(col)
        axs3[i].set_title(col)
        if i >= n_cols * n_rows - 1:
            break

    fig.tight_layout()
    fig.savefig('out/figures/seasonal_decomposition.png')
    fig2.tight_layout()
    fig2.savefig('out/figures/trend_seasonal_decomposition.png')
    fig3.tight_layout()
    fig3.savefig('out/figures/resid_seasonal_decomposition.png')
    if SHOW_PLOT:
        plt.show()


def plot_preliminary_data(df) -> None:
    plot_differencing(df)
    plot_correlation(df)
    plot_seasonal_decompose(df)


def plot_prediction(
        baseline: pd.DataFrame,
        actual_values: pd.DataFrame,
        model_name: str,
        model_predictions: pd.DataFrame
) -> None:
    n_cols = len(model_predictions.columns) - 1
    n_rows = math.ceil(n_cols / 8)
    n_cols = min(n_cols, 8)
    fig, axs = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(20, 15))
    axs = axs.ravel()[:len(model_predictions.columns) - 1]
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)

    fig.suptitle(model_name)
    for i, col in enumerate(model_predictions.columns[1:]):
        axs[i].plot(baseline[col], label="Baseline", color="orange")
        axs[i].plot(actual_values.iloc[:, i + 1], label="Actual", color="blue")
        axs[i].plot(model_predictions[col], label=model_name, color="red")
        axs[i].legend()
        axs[i].set_title(col)

    plt.tight_layout()
    plt.savefig(f'out/figures/{model_name}_comparison.png')
    if SHOW_PLOT:
        plt.show()


def main():
    # ------------------------------------------------------------------------------------------------------------------
    # Load Data
    secret_training_data = pd.read_csv(Path("datasets") / "secret_training_data.csv")
    plot_preliminary_data(secret_training_data)
    baseline = pd.DataFrame(pd.read_csv(Path("out/model_selection") / "BaseLine-Predictions.csv"))
    actual_values = pd.DataFrame(pd.read_csv(Path("out/model_selection") / "TRUE-SEQUENCES.csv"))
    predictions = {
        "Auto Arima": pd.DataFrame(pd.read_csv(Path("out/model_selection") / "AutoArima-Predictions.csv")),
        "FeedForward 10": pd.DataFrame(pd.read_csv(Path("out/model_selection") / "FeedForward-10-Predictions.csv")),
        "FeedForward 50": pd.DataFrame(pd.read_csv(Path("out/model_selection") / "FeedForward-50-Predictions.csv")),
        "FeedForward 150": pd.DataFrame(pd.read_csv(Path("out/model_selection") / "FeedForward-150-Predictions.csv")),
        "FeedForward 200": pd.DataFrame(pd.read_csv(Path("out/model_selection") / "FeedForward-200-Predictions.csv")),
        "LSTM 64": pd.DataFrame(pd.read_csv(Path("out/model_selection") / "LSTM-seq10-hu64-Predictions.csv")),
        "LSTM 128": pd.DataFrame(pd.read_csv(Path("out/model_selection") / "LSTM-seq10-hu128-Predictions.csv")),
    }
    # ------------------------------------------------------------------------------------------------------------------
    for model, data in predictions.items():
        plot_prediction(baseline, actual_values, model, data)
    # ------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":
    if len(sys.argv) > 1:
        SHOW_PLOT = sys.argv[1] == "-sp"
    main()
