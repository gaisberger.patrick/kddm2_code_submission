# KDDM-2 Forecasting Challange
Authors: 
- Patrick Gaisberger (Mat Nr.: 11703581)
- Muhammed Durakovic (Mat Nr.: 11904556)
- Delal Ucar (Mat Nr.: 01531765)

## Install Dependencies 
Please install the dependencies with [poetry](https://python-poetry.org/).
For the specific versions of the libraries which we have used, have a look at [pyproject.toml](pyproject.toml).

## Run Scripts

### Model-Selection-[Script](model_selection_main.py) 

Run this script with `python3 model_selection_main.py [sequences.csv]`

I.e. `python3 model_selection_main.py datasets/training_data.csv`

The given sequences file should contain one column with the name `times` and at least one column with sequences. 
For reference have a look at this [file](datasets/training_data.csv). 

This will:
- trains multiple models on 80% of the `[sequences.csv]` dataset and test on the rest. 
- create [FOLDER](out/model_selection) containing 
  - the predictions of the respective models, 
  - the [training history](out/model_selection/history) of the respective models (if available)
  - the [results](out/model_selection/out.txt) of the model-selection


### Forecast-[Script](forecast_main.py)

Run this script with `python3 forecast_main.py [model] [out-predictions-file]`

For example `python3 forecast_main.py arima group1_final.csv`

This will:

- Train the respective `[model]`  (`arima`, `baseline`, `feedforward` or `lstm`) on the [secret_training_data.csv](datasets/secret_training_data.csv)
- predict for the respective times in the [public_fake_test_data](datasets/public_fake_test_data.csv)
- save the predictions in `[out-predictions-file]`

### Plot-[Script](plot_model_selection_results_main.py)

Run this script with `python3 plot_factory_main.py [-sp]`. `-sp` to show the plots.
This only works if you've run `python3 model_selection_main.py datasets/secret_training_data.csv` b.

This will:
- explore the final dataset on which our models were trained on
- plot First Order Differenecing for each timeseries
- plot Correlation of data for each timeseries
- plot the Seasonal Decomposition for each timeseries
- and finally plot a Comparison of Predictions between Baseline and other Models
- every figure is saved in [FOLDER](out/figures) 

### Conclusion


We've tried out different parameters for the neural nets and ran arima with plain data. 
The baseline model calculates the mean of each sequence and uses it as prediction. 
Our current solution is to reduce the data to the median of a month and predict the median with arima. 
The daily data is then taken along the slope of the two closest months. 
Arima with this approach yielded the best [results](out/model_selection/out_secret_training.txt).

- Error: mean-squared-distance
- Score: mean inverse of the logarithmic distance

The final forecast: [file](group1_final.csv)