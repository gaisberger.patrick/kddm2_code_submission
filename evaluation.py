from pathlib import Path
from typing import List, Any

import numpy as np
import pandas as pd
from numpy import typing as npt
from sklearn.metrics import mean_squared_error


def error(y_true: npt.NDArray[Any], y: npt.NDArray[Any]) -> float:
    return mean_squared_error(y_true, y)


def score(y_true: npt.NDArray[Any], y: npt.NDArray[Any]) -> float:
    """Score described on https://courses.isds.tugraz.at/rkern/courses/kddm2/"""
    return np.mean(1 / np.log(np.sqrt(np.sum((y_true - y) ** 2, axis=1))))  # type: ignore


def save_predictions(predictions: npt.NDArray[Any], times: npt.NDArray[str], cols: List[str], file_path: Path) -> None:
    """Saves the predictions"""
    if "times" in cols:
        cols = cols.remove("times")

    prediction_df = pd.DataFrame(data=predictions, columns=cols)
    prediction_df.insert(loc=0, column="times", value=times)
    file_path.parent.mkdir(parents=True, exist_ok=True)
    prediction_df.to_csv(file_path, index=False)
