# This is a sample Python script.
import sys

import torch

# enable gpu if available
torch.device('cuda' if torch.cuda.is_available() else 'cpu')

import copy
from pathlib import Path
from typing import List, Tuple

import pandas as pd
from pandas import DataFrame
from sklearn.model_selection import train_test_split

from evaluation import error, score, save_predictions
from models import TimeSeriesModel, BaseLineModel, FeedForward, LSTMCustom, AutoArima


def main(train_data_path:Path):
    out_directory = Path("out") / "model_selection"
    out_directory.mkdir(parents=True, exist_ok=True)

    data_frame: DataFrame = pd.read_csv(train_data_path)
    model_hist_out_dir = out_directory / "history"
    model_list: List[Tuple[str, TimeSeriesModel]] = [
        # --------------------------------------------------------------------------------------------------------------
        ("BaseLine", BaseLineModel()),
        # --------------------------------------------------------------------------------------------------------------
        # Simple Feed Forward Model (We know it won't produce good predictions) ----------------------------------------
        ("FeedForward-10", FeedForward(n_time_series=len(data_frame.columns) - 1, sequence_length=10,
                                       train_history_path=model_hist_out_dir / "FeedForward-10-HISTORY.csv")),
        ("FeedForward-50", FeedForward(n_time_series=len(data_frame.columns) - 1, sequence_length=50,
                                       train_history_path=model_hist_out_dir / "FeedForward-50-HISTORY.csv")),
        ("FeedForward-150", FeedForward(n_time_series=len(data_frame.columns) - 1, sequence_length=150,
                                        train_history_path=model_hist_out_dir / "FeedForward-150-HISTORY.csv")),
        ("FeedForward-200", FeedForward(n_time_series=len(data_frame.columns) - 1, sequence_length=200,
                                        train_history_path=model_hist_out_dir / "FeedForward-200-HISTORY.csv")),
        # --------------------------------------------------------------------------------------------------------------
        # LSTM Model from https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html ---------------------------------
        ("LSTM-seq10-hu64", LSTMCustom(n_time_series=len(data_frame.columns) - 1, sequence_length=10, hidden_units=64,
                                       train_history_path=model_hist_out_dir / "LSTM-seq10-hu64-HISTORY.csv")),
        ("LSTM-seq10-hu128", LSTMCustom(n_time_series=len(data_frame.columns) - 1, sequence_length=10, hidden_units=128,
                                        train_history_path=model_hist_out_dir / "LSTM-seq10-hu128-HISTORY.csv")),
        # --------------------------------------------------------------------------------------------------------------
        # AutoArima https://alkaline-ml.com/pmdarima/index.html --------------------------------------------------------
        ("AutoArima", AutoArima()),
        # --------------------------------------------------------------------------------------------------------------
    ]
    train_data, test_data = train_test_split(data_frame, train_size=0.8, shuffle=False)  # type: DataFrame, DataFrame
    test_data.to_csv(out_directory / "TRUE-SEQUENCES.csv", index=False)
    print(250 * "-")
    for name, model in model_list:
        print("Run model:", name)
        model.fit(copy.deepcopy(train_data))
        pred = model.predict(test_data.shape[0])
        y_true = TimeSeriesModel.default_preprocess(test_data)
        save_predictions(
            predictions=pred,
            times=test_data.times.tolist(),
            cols=data_frame.columns.tolist(),
            file_path=out_directory / f"{name}-Predictions.csv"
        )
        with open(out_directory / "out.txt", "a") as file_writer:
            print(f"{name}-Model error: {error(y_true, pred):.3f}, score: {score(y_true, pred):.3f}", file=file_writer)
        print(250 * "-")


if __name__ == '__main__':
    main(Path(sys.argv[1]))
